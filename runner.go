package main

import (
	"errors"
	"fmt"
	"strings"

	"github.com/bgentry/speakeasy"
)

type Runner struct {
	KustomizationFile string
	Exclude           string
	Profile           string
	DependsOn         string
	SaveResult        bool
	ShowResult        bool
	resolver          *Resolver
	profile           *ResolverProfile
	kustomizations    map[string]*KustomizationFile
}

func (r *Runner) IsActionSpecified() bool {
	if len(r.DependsOn) == 0 && !r.SaveResult && !r.ShowResult {
		return false
	}
	return true
}

func (r *Runner) setProfile() error {
	profile := NewResolverProfile()
	if len(r.Profile) > 0 {
		builtinProfile, ok := GetBuiltinProfile(r.Profile)
		if ok {
			profile = builtinProfile
		} else {
			err := profile.LoadFromFile(r.Profile)
			if err != nil {
				return err
			}
		}
	}
	profile.LoadFromEnv()
	r.profile = profile
	return nil
}

func (r *Runner) Setup(allowAsk bool) error {
	if err := r.setProfile(); err != nil {
		return err
	}

	if len(r.Exclude) != 0 {
		r.profile.Exclude = r.Exclude
	}

	if allowAsk && !r.profile.GitLabConfig.HasValidAuth() {
		err := r.askForGitLabCreds()
		if err != nil {
			return err
		}
	}

	resolver, err := NewResolver(r.profile)
	if err != nil {
		return err
	}
	r.resolver = resolver

	kustomizations, err := r.resolver.ResolveFile(r.KustomizationFile)
	if err != nil {
		return err
	}
	r.kustomizations = kustomizations

	if len(r.kustomizations) == 0 {
		return errors.New("Empty list of kustomization files")
	}

	return nil
}

func (r *Runner) askForGitLabCreds() error {
	fmt.Print("Enter GitLab username: ")
	fmt.Scanln(&r.profile.GitLabConfig.Username)
	password, err := speakeasy.Ask("Enter GitLab password: ")
	if err != nil {
		return err
	}
	r.profile.GitLabConfig.Password = password
	return nil
}

func (r *Runner) saveKustomizations() error {
	for path, kustomization := range r.kustomizations {
		if err := kustomization.SaveTo(path); err != nil {
			return err
		}
	}
	return nil
}

func (r *Runner) printsKustomizations() error {
	for path, kustomization := range r.kustomizations {
		res, err := kustomization.String()
		if err != nil {
			return err
		}
		fmt.Printf("# %s\n", path)
		fmt.Println(res)
	}
	return nil
}

func (r *Runner) runsTemplateHooks() error {
	t := EmptyTemplate()
	t.SetHostEnvvars()
	t.SetBases(r.resolver.bases)
	t.SetTemplateEnvvars(r.profile.PostTemplateVars)
	for tmplFrom, tmplTo := range r.profile.PostTemplateHooks {
		t.SetTemplateFile(tmplFrom)
		err := t.RenderToFile(tmplFrom, tmplTo)
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *Runner) Run() error {
	if r.SaveResult {
		err := r.saveKustomizations()
		if err != nil {
			return err
		}
	}

	if r.ShowResult {
		err := r.printsKustomizations()
		if err != nil {
			return err
		}
	}

	if len(r.DependsOn) > 0 {
		deps := r.resolver.DependsOn(r.DependsOn)
		if len(deps) > 0 {
			fmt.Println(strings.Join(deps, " "))
		}
	}

	if len(r.profile.PostTemplateHooks) > 0 {
		err := r.runsTemplateHooks()
		if err != nil {
			return err
		}
	}

	return nil
}
