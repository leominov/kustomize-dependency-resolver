package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"text/template"
)

var (
	templateFuncMap = template.FuncMap{
		"ToUpper":    strings.ToUpper,
		"ToLower":    strings.ToLower,
		"TrimPrefix": strings.TrimPrefix,
		"TrimSuffix": strings.TrimSuffix,
		"TrimSpace":  strings.TrimSpace,
		"Replace":    strings.Replace,
		"Contains":   strings.Contains,
	}
)

type Template struct {
	mu           *sync.RWMutex
	templateFile string
	templateEnv  map[string]interface{}
	hostEnv      map[string]string
	bases        []*Base
}

func EmptyTemplate() *Template {
	return &Template{
		mu: &sync.RWMutex{},
	}
}

func (t *Template) SetHostEnvvars() *Template {
	fullHostEnv := os.Environ()
	hostEnvMap := make(map[string]string, len(fullHostEnv))
	for _, e := range fullHostEnv {
		parts := strings.SplitN(e, "=", 2)
		key, value := parts[0], parts[1]
		hostEnvMap[key] = value
	}
	t.mu.Lock()
	t.hostEnv = hostEnvMap
	t.mu.Unlock()
	return t
}

func (t *Template) SetTemplateEnvvars(vars map[string]interface{}) *Template {
	t.mu.Lock()
	t.templateEnv = vars
	t.mu.Unlock()
	return t
}

func (t *Template) SetTemplateFile(path string) *Template {
	t.mu.Lock()
	t.templateFile = path
	t.mu.Unlock()
	return t
}

func (t *Template) SetBases(bases []*Base) *Template {
	t.mu.Lock()
	t.bases = bases
	t.mu.Unlock()
	return t
}

func (t *Template) readTextFile(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(b), err
}

func (t *Template) Render(name string) (string, error) {
	var templateEng *template.Template
	buf := bytes.NewBufferString("")
	templateEng = template.New(name).Option("missingkey=zero")
	t.mu.RLock()
	defer t.mu.RUnlock()
	data := map[string]interface{}{
		"env":   t.hostEnv,
		"bases": t.bases,
		"vars":  t.templateEnv,
	}
	templ, err := t.readTextFile(t.templateFile)
	if err != nil {
		return "", err
	}
	if messageTempl, err := templateEng.Funcs(templateFuncMap).Parse(templ); err != nil {
		return "", fmt.Errorf("failed to parse template for %s: %v", name, err)
	} else if err := messageTempl.Execute(buf, data); err != nil {
		return "", fmt.Errorf("failed to execute template for %s: %v", name, err)
	}
	return buf.String(), nil
}

func (t *Template) RenderToFile(name, filepath string) error {
	if len(filepath) == 0 {
		filepath = t.templateFile
	}
	out, err := t.Render(name)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filepath, []byte(out), os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}
