# Kustomize dependency resolver

From zero:

```
# serviceA kustomization.yaml
bases:
- git::https://localhost/group/serviceB.git//kustomize/base?ref=master
- git::https://localhost/infra/depA.git//kustomize/base?ref=master
- base
```

```
# serviceB kustomization.yaml
bases:
- git::https://localhost/group/serviceC.git//kustomize/base?ref=master
- git::https://localhost/infra/depB.git//kustomize/base?ref=master
- base
```

```
# serviceC kustomization.yaml
bases:
- git::https://localhost/group/serviceA.git//kustomize/base?ref=master
- git::https://localhost/infra/depC.git//kustomize/base?ref=master
- base
```

To hero:

```
# serviceA kustomization.yaml with all dependencies
bases:
- git::https://localhost/infra/depA.git//kustomize/overlays/size_A?ref=master
- git::https://localhost/infra/depB.git//kustomize/overlays/size_B?ref=master
- git::https://localhost/infra/depC.git//kustomize/overlays/size_C?ref=master
- git::https://localhost/group/serviceB.git//kustomize/overlays/testing?ref=master
- git::https://localhost/group/serviceC.git//kustomize/overlays/testing?ref=master
- kustomize/overlays/testing
```
