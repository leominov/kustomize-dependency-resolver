package main

import "testing"

func TestParseEntry(t *testing.T) {
	_, err := BaseFromEntry("git::123:")
	if err == nil {
		t.Error("Must return error, but got nil")
	}

	_, err = BaseFromEntry("base")
	if err == nil {
		t.Error("Must return error, but got nil")
	}

	_, err = BaseFromEntry("http://yandex.ru")
	if err == nil {
		t.Error("Must return error, but got nil")
	}

	b1, err := BaseFromEntry("git::https://localhost/group/service.git//kustomize/base/?ref=master")
	if err != nil {
		t.Error("Must return nil, but got error")
	}
	if b1.Raw != "git::https://localhost/group/service.git//kustomize/base/?ref=master" {
		t.Errorf("Unexpected result: %s", b1.Raw)
	}
	if b1.Name != "service" {
		t.Errorf("Unexpected result: %s", b1.Name)
	}
	if b1.Ref != "master" {
		t.Errorf("Unexpected result: %s", b1.Raw)
	}
	if b1.Overlay != "kustomize/base" {
		t.Errorf("Unexpected result: %s", b1.Overlay)
	}
	if b1.Namespace != "group/service" {
		t.Errorf("Unexpected result: %s", b1.Overlay)
	}
	if b1.String() != "git::https://localhost/group/service.git//kustomize/base?ref=master" {
		t.Errorf("Unexpected result: %s", b1.String())
	}

	b2, err := BaseFromEntry("git::https://localhost/group/service.git//kustomize/base")
	if err != nil {
		t.Error("Must return nil, but got error")
	}
	if b2.Ref != "master" {
		t.Errorf("Unexpected result: %s", b2.Raw)
	}
	if b2.String() != "git::https://localhost/group/service.git//kustomize/base?ref=master" {
		t.Errorf("Unexpected result: %s", b1.String())
	}

	_, err = BaseFromEntry("git::https://localhost/group/service.git")
	if err == nil {
		t.Error("Must return error, but got nil")
	}

	_, err = BaseFromEntry("git::https://localhost/group/service.git//?ref=master")
	if err == nil {
		t.Error("Must return error, but got nil")
	}
}
