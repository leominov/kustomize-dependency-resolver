package main

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

const (
	DefaultDependencyMaxDepth = 2
)

type ResolverProfile struct {
	Exclude               string                 `yaml:"exclude"`
	DependencyMaxDepth    int                    `yaml:"dependencyMaxDepth"`
	GitLabConfig          *GitLabConfig          `yaml:"gitlab"`
	BasesDefault          []string               `yaml:"basesDefault"`
	OutputMap             map[string]string      `yaml:"outputMap"`
	BasesDependencySkip   []string               `yaml:"basesDependencySkip"`
	BasesSkip             []string               `yaml:"basesSkip"`
	OverlayReplaceMap     map[string]string      `yaml:"overlayReplaceMap"`
	OverlayDependencyPath string                 `yaml:"overlayDependencyPath"`
	PostTemplateVars      map[string]interface{} `yaml:"postTemplateVars"`
	PostTemplateHooks     map[string]string      `yaml:"postTemplateHooks"`
}

type GitLabConfig struct {
	Insecure bool   `yaml:"insecure"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Endpoint string `yaml:"endpoint"`
	Token    string `yaml:"token"`
}

func NewResolverProfile() *ResolverProfile {
	r := &ResolverProfile{
		GitLabConfig:      &GitLabConfig{},
		OverlayReplaceMap: make(map[string]string),
		OutputMap:         make(map[string]string),
	}
	r.SetDefaults()
	return r
}

func (g *GitLabConfig) HasValidAuth() bool {
	if len(g.Token) > 0 {
		return true
	}
	if len(g.Username) > 0 && len(g.Password) > 0 {
		return true
	}
	return false
}

func (r *ResolverProfile) SetDefaults() {
	if r.DependencyMaxDepth == 0 {
		r.DependencyMaxDepth = DefaultDependencyMaxDepth
	}
	if len(r.OutputMap) == 0 {
		r.OutputMap = map[string]string{
			"*": KustomizationFilename,
		}
	}
	if len(r.OverlayReplaceMap) == 0 {
		r.OverlayReplaceMap = make(map[string]string)
	}
}

func (r *ResolverProfile) LoadFromFile(path string) error {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(b, &r); err != nil {
		return err
	}
	return err
}

func (r *ResolverProfile) LoadFromEnv() {
	gitlabPassword := os.Getenv("GITLAB_PASSWORD")
	if len(gitlabPassword) > 0 {
		r.GitLabConfig.Password = gitlabPassword
	}
	gitlabToken := os.Getenv("GITLAB_TOKEN")
	if len(gitlabToken) > 0 {
		r.GitLabConfig.Token = gitlabToken
	}
	gitlabUser := os.Getenv("GITLAB_USER")
	if len(gitlabUser) > 0 {
		r.GitLabConfig.Username = gitlabUser
	}
	gitlabEndpoint := os.Getenv("GITLAB_URL")
	if len(gitlabEndpoint) > 0 {
		r.GitLabConfig.Endpoint = gitlabEndpoint
	}
}
