package main

import (
	"os"
	"testing"
)

func TestNewResolverProfile(t *testing.T) {
	r := NewResolverProfile()
	if r.GitLabConfig == nil {
		t.Error("Must return object, but got nil")
	}
	if len(r.BasesDefault) != 0 {
		t.Errorf("Must return 0, but got %d", len(r.BasesDefault))
	}
	if r.DependencyMaxDepth != DefaultDependencyMaxDepth {
		t.Errorf("Must return %d, but got %d", DefaultDependencyMaxDepth, r.DependencyMaxDepth)
	}
	if len(r.OutputMap) != 1 {
		t.Errorf("Must return 1, but got %d", len(r.OutputMap))
	}
	r2 := NewResolverProfile()
	r2.OutputMap = map[string]string{
		"*": "output.yaml",
	}
	if len(r2.OutputMap) != 1 {
		t.Errorf("Must return 1, but got %d", len(r2.OutputMap))
	}
}

func TestLoadFromFile(t *testing.T) {
	r := NewResolverProfile()
	if err := r.LoadFromFile("test_data/notfound-invalid.yaml"); err == nil {
		t.Error("Must return error, but got nil")
	}
	if err := r.LoadFromFile("test_data/profile-invalid.yaml"); err == nil {
		t.Error("Must return error, but got nil")
	}
	if err := r.LoadFromFile("test_data/profile-valid.yaml"); err != nil {
		t.Error("Must return nil, but got error")
	}
}

func TestHasValidAuth(t *testing.T) {
	var (
		config *GitLabConfig
		ok     bool
	)
	config = &GitLabConfig{}
	ok = config.HasValidAuth()
	if ok {
		t.Error("Must be false, but got true")
	}
	config = &GitLabConfig{
		Token: "token",
	}
	ok = config.HasValidAuth()
	if !ok {
		t.Error("Must be true, but got false")
	}
	config = &GitLabConfig{
		Username: "username",
	}
	ok = config.HasValidAuth()
	if ok {
		t.Error("Must be false, but got true")
	}
	config = &GitLabConfig{
		Password: "password",
	}
	ok = config.HasValidAuth()
	if ok {
		t.Error("Must be false, but got true")
	}
	config = &GitLabConfig{
		Username: "username",
		Password: "password",
	}
	ok = config.HasValidAuth()
	if !ok {
		t.Error("Must be true, but got false")
	}
}

func TestLoadFromEnv(t *testing.T) {
	cases := map[string]string{
		"GITLAB_PASSWORD": "password",
		"GITLAB_TOKEN":    "token",
		"GITLAB_USER":     "user",
		"GITLAB_URL":      "url",
	}
	for _, env := range cases {
		os.Unsetenv(env)
	}
	r := NewResolverProfile()
	r.LoadFromEnv()
	if r.GitLabConfig.Password != "" {
		t.Errorf("Must be empty, but go %s", r.GitLabConfig.Password)
	}
	if r.GitLabConfig.Token != "" {
		t.Errorf("Must be empty, but go %s", r.GitLabConfig.Token)
	}
	if r.GitLabConfig.Username != "" {
		t.Errorf("Must be empty, but go %s", r.GitLabConfig.Username)
	}
	if r.GitLabConfig.Endpoint != "" {
		t.Errorf("Must be empty, but go %s", r.GitLabConfig.Endpoint)
	}
	for env, val := range cases {
		os.Setenv(env, val)
	}
	r.LoadFromEnv()
	if r.GitLabConfig.Password != cases["GITLAB_PASSWORD"] {
		t.Errorf("Must be %s, but go %s", cases["GITLAB_PASSWORD"], r.GitLabConfig.Password)
	}
	if r.GitLabConfig.Token != cases["GITLAB_TOKEN"] {
		t.Errorf("Must be %s, but go %s", cases["GITLAB_TOKEN"], r.GitLabConfig.Token)
	}
	if r.GitLabConfig.Username != cases["GITLAB_USER"] {
		t.Errorf("Must be %s, but go %s", cases["GITLAB_USER"], r.GitLabConfig.Username)
	}
	if r.GitLabConfig.Endpoint != cases["GITLAB_URL"] {
		t.Errorf("Must be %s, but go %s", cases["GITLAB_URL"], r.GitLabConfig.Endpoint)
	}
}
