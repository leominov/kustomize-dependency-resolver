package main

import "testing"

func TestIsInStrSlice(t *testing.T) {
	tests := []struct {
		inSlice  bool
		strSlice []string
	}{
		{
			inSlice:  false,
			strSlice: []string{},
		},
		{
			inSlice:  false,
			strSlice: []string{"barfoo"},
		},
		{
			inSlice:  true,
			strSlice: []string{"foobar"},
		},
	}
	for _, test := range tests {
		ok := IsInStrSlice("foobar", test.strSlice)
		if ok != test.inSlice {
			t.Errorf("Must be %v, but got %v", test.inSlice, ok)
		}
	}
}
