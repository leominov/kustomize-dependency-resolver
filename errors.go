package main

import "errors"

var (
	ErrSkipped = errors.New("Skipped")
)
