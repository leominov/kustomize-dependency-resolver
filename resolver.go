package main

import (
	"crypto/tls"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"

	"github.com/xanzy/go-gitlab"
)

type Resolver struct {
	profile    *ResolverProfile
	kinput     *KustomizationFile
	gitlabCli  *gitlab.Client
	sourceFile string
	bases      []*Base
	projects   map[string]*Project
}

func NewResolver(profile *ResolverProfile) (*Resolver, error) {
	var (
		err       error
		gitlabCli *gitlab.Client
	)
	httpCli := http.DefaultClient
	if profile.GitLabConfig.Insecure {
		httpCli = &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		}
	}
	if len(profile.GitLabConfig.Token) > 0 {
		gitlabCli = gitlab.NewClient(
			httpCli,
			profile.GitLabConfig.Token,
		)
		if err := gitlabCli.SetBaseURL(profile.GitLabConfig.Endpoint); err != nil {
			return nil, err
		}
	} else {
		gitlabCli, err = gitlab.NewBasicAuthClient(
			httpCli,
			profile.GitLabConfig.Endpoint,
			profile.GitLabConfig.Username,
			profile.GitLabConfig.Password,
		)
	}
	if err != nil {
		return nil, fmt.Errorf("Error creating GitLab client: %v", err)
	}
	r := &Resolver{
		profile:   profile,
		gitlabCli: gitlabCli,
		bases:     []*Base{},
		projects:  make(map[string]*Project),
	}
	return r, nil
}

func (r *Resolver) DependsOn(name string) []string {
	result := []string{}
	for _, project := range r.GetProjects() {
		for _, link := range project.Links {
			if link.Name == name {
				result = append(result, project.Base.Name)
			}
		}
	}
	return result
}

func (r *Resolver) IsExcludedEntry(link string) bool {
	for _, e := range r.profile.BasesDefault {
		if link == e {
			return true
		}
	}
	return false
}

func (r *Resolver) ResolveFile(path string) (map[string]*KustomizationFile, error) {
	var (
		wg     sync.WaitGroup
		failed bool
	)
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	kinput := &KustomizationFile{}
	if err := kinput.Parse(b); err != nil {
		return nil, err
	}
	r.sourceFile = path
	r.kinput = kinput
	failed = false
	wg.Add(len(kinput.Bases))
	for _, base := range kinput.Bases {
		go func(base string) {
			err := r.addProjectBases([]string{base}, "root", r.profile.DependencyMaxDepth)
			if err != nil {
				logrus.Error(err)
				failed = true
			}
			wg.Done()
		}(base)
	}
	wg.Wait()
	if failed {
		return nil, errors.New("Failed")
	}
	koutput := r.GetKustomizationRepresentation()
	return koutput, nil
}

func (r *Resolver) GetBases() []*Base {
	return r.bases
}

func (r *Resolver) GetProjects() map[string]*Project {
	return r.projects
}

func (r *Resolver) AddBase(base *Base) {
	if len(r.profile.Exclude) > 0 && strings.Contains(base.String(), r.profile.Exclude) {
		return
	}
	for dest, rep := range r.profile.OverlayReplaceMap {
		if strings.Contains(base.Namespace, dest) {
			base.Overlay = rep
		}
	}
	for _, b := range r.bases {
		if b.String() == base.String() {
			return
		}
	}
	r.bases = append(r.bases, base)
}

func (r *Resolver) IsProjectExists(ns string) bool {
	if _, ok := r.projects[ns]; ok {
		return true
	}
	return false
}

func (r *Resolver) AddProject(p *Project) {
	if r.IsProjectExists(p.Base.Namespace) {
		return
	}
	r.projects[p.Base.Namespace] = p
}

func (r *Resolver) GetProject(ns string) *Project {
	if p, ok := r.projects[ns]; ok {
		return p
	}
	return nil
}

func (r *Resolver) getBaseProject(base string) (*Project, error) {
	if r.IsExcludedEntry(base) {
		return nil, ErrSkipped
	}
	b, err := BaseFromEntry(base)
	if err != nil {
		return nil, err
	}
	if project := r.GetProject(b.Namespace); project != nil {
		return project, nil
	}
	p, _, err := r.gitlabCli.Projects.GetProject(b.Namespace)
	if err != nil {
		return nil, err
	}
	return &Project{
		Project: p,
		Base:    b,
	}, nil
}

func (r *Resolver) addProjectBases(bases []string, parent string, maxDepth int) error {
	if maxDepth == 0 {
		return nil
	}
	maxDepth--
	for _, base := range bases {
		if IsInStrSlice(base, r.profile.BasesSkip) {
			continue
		}
		project, err := r.getBaseProject(base)
		if err != nil && err != ErrSkipped {
			return fmt.Errorf("Error with %s: %v", base, err)
		}
		if project == nil {
			continue
		}
		if p, ok := r.projects[parent]; ok {
			p.Links = append(p.Links, project.Base)
		}
		if r.IsProjectExists(project.Base.Namespace) {
			continue
		}
		r.AddBase(project.Base)
		if IsInStrSlice(base, r.profile.BasesDependencySkip) {
			continue
		}
		r.AddProject(project)
		includedFile, err := r.getProjectKustomizationFile(project)
		if err != nil {
			return fmt.Errorf("Error getting %s dependencies: %v", project.Base.Namespace, err)
		}
		return r.addProjectBases(includedFile.Bases, project.Base.Namespace, maxDepth)
	}
	return nil
}

func (r *Resolver) getProjectKustomizationFile(project *Project) (*KustomizationFile, error) {
	options := &gitlab.GetFileOptions{
		Ref: gitlab.String(project.Base.Ref),
	}
	kpath := path.Join(r.profile.OverlayDependencyPath, KustomizationFilename)
	gfile, _, err := r.gitlabCli.RepositoryFiles.GetFile(project.Base.Namespace, kpath, options)
	if err != nil {
		return nil, err
	}
	b, err := base64.StdEncoding.DecodeString(gfile.Content)
	if err != nil {
		return nil, err
	}
	k := &KustomizationFile{}
	if err := k.Parse(b); err != nil {
		return nil, err
	}
	return k, nil
}

func (r *Resolver) GetKustomizationRepresentation() map[string]*KustomizationFile {
	result := make(map[string]*KustomizationFile)
	defOut := r.profile.OutputMap["*"]
	for _, out := range r.profile.OutputMap {
		result[out] = &KustomizationFile{
			Bases:            []string{},
			GeneratorOptions: r.kinput.GeneratorOptions,
		}
	}
	for _, base := range r.bases {
		match := false
		for ns, out := range r.profile.OutputMap {
			if strings.Contains(base.Namespace, ns) {
				result[out].Bases = append(result[out].Bases, base.String())
				match = true
			}
		}
		if match {
			continue
		}
		result[defOut].Bases = append(result[defOut].Bases, base.String())
	}
	result[defOut].Bases = append(result[defOut].Bases, r.profile.BasesDefault...)
	return result
}
