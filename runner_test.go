package main

import "testing"

func TestIsActionSpecified(t *testing.T) {
	r := &Runner{}
	if r.IsActionSpecified() {
		t.Errorf("Must be false, but got true")
	}
	r = &Runner{
		DependsOn: "foobar",
	}
	if !r.IsActionSpecified() {
		t.Errorf("Must be true, but got false")
	}
	r = &Runner{
		SaveResult: true,
	}
	if !r.IsActionSpecified() {
		t.Errorf("Must be true, but got false")
	}
	r = &Runner{
		ShowResult: true,
	}
	if !r.IsActionSpecified() {
		t.Errorf("Must be true, but got false")
	}
}
