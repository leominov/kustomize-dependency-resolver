package main

import (
	"github.com/xanzy/go-gitlab"
)

type Project struct {
	Project *gitlab.Project
	Links   []*Base
	Base    *Base
}
