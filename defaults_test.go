package main

import (
	"strings"
	"testing"
)

func TestGetBuiltinProfile(t *testing.T) {
	BuiltinProfiles = nil
	profile, ok := GetBuiltinProfile("name")
	if ok {
		t.Error("Must return false, but got true")
	}
	if profile != nil {
		t.Errorf("Must return nil, but got %#v", profile)
	}
	BuiltinProfiles = map[string]*ResolverProfile{
		"name": &ResolverProfile{
			Exclude: "path",
		},
	}
	profile, ok = GetBuiltinProfile("name")
	if !ok {
		t.Error("Must return true, but got false")
	}
	if profile == nil {
		t.Error("Must return object, but got nil")
	}
	if profile.Exclude != "path" {
		t.Errorf("Must return 'path', but got %s", profile.Exclude)
	}
	profile, ok = GetBuiltinProfile("not-found")
	if ok {
		t.Error("Must return false, but got true")
	}
	if profile != nil {
		t.Errorf("Must return nil, but got %#v", profile)
	}
}

func TestBuiltinProfilesNames(t *testing.T) {
	BuiltinProfiles = nil
	name := BuiltinProfilesNames()
	if name != "<none>" {
		t.Errorf("Must return '<none>', but got '%s'", name)
	}
	BuiltinProfiles = map[string]*ResolverProfile{
		"name1": &ResolverProfile{},
		"name2": &ResolverProfile{},
	}
	name = BuiltinProfilesNames()
	if !strings.Contains(name, ", ") {
		t.Errorf("Must contains ', ', but got '%s'", name)
	}
	if !strings.Contains(name, "name1") {
		t.Errorf("Must contains 'name1', but got '%s'", name)
	}
	if !strings.Contains(name, "name2") {
		t.Errorf("Must contains 'name2', but got '%s'", name)
	}
}
