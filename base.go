package main

import (
	"errors"
	"fmt"
	"net/url"
	"path"
	"strings"
)

var (
	entryPrefix = "git::"
)

type Base struct {
	Raw       string
	Ref       string
	Overlay   string
	URL       *url.URL
	Namespace string
	Group     string
	Name      string
}

func BaseFromEntry(link string) (*Base, error) {
	b := &Base{
		Raw: link,
	}
	if !strings.HasPrefix(link, entryPrefix) {
		return nil, ErrSkipped
	}
	link = strings.TrimPrefix(link, entryPrefix)
	link = strings.Replace(link, ".git", "", 1)
	u, err := url.Parse(link)
	if err != nil {
		return nil, err
	}
	ref := u.Query().Get("ref")
	if len(ref) == 0 {
		ref = "master"
	}
	pathArr := strings.Split(u.Path, "//")
	if len(pathArr) != 2 {
		return nil, errors.New("Overlay must be specified")
	}

	namespace := pathArr[0]
	namespace = strings.Trim(namespace, "/")
	b.Namespace = namespace
	b.Group = path.Dir(namespace)
	b.Name = path.Base(namespace)

	overlay := pathArr[1]
	if len(overlay) == 0 {
		return nil, errors.New("Overlay must be specified and non empty")
	}
	overlay = strings.Trim(overlay, "/")
	b.Overlay = overlay

	b.Ref = ref
	b.URL = u
	return b, nil
}

func (b *Base) String() string {
	return fmt.Sprintf(
		"%s%s://%s/%s.git//%s?ref=%s",
		entryPrefix,
		b.URL.Scheme,
		b.URL.Host,
		b.Namespace,
		b.Overlay,
		b.Ref,
	)
}
