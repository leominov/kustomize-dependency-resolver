package main

import "strings"

func IsInStrSlice(name string, arr []string) bool {
	if len(arr) == 0 {
		return false
	}
	for _, val := range arr {
		if strings.Contains(name, val) {
			return true
		}
	}
	return false
}
