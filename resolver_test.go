package main

import "testing"

func TestAddBase(t *testing.T) {
	r := &Resolver{
		bases: []*Base{},
		profile: &ResolverProfile{
			Exclude: "foobar",
			OverlayReplaceMap: map[string]string{
				"group/project": "new/overlay",
			},
		},
	}
	b, err := BaseFromEntry("git::https://localhost/group/project.git//base")
	if err != nil {
		t.Errorf("Must return nil, but got error: %v", err)
	}
	r.AddBase(b)
	if len(r.bases) != 1 {
		t.Errorf("Must return 1, but got %d", len(r.bases))
	}
	if r.bases[0].String() != "git::https://localhost/group/project.git//new/overlay?ref=master" {
		t.Errorf("Unexpected result: %s", r.bases[0])
	}
	r.bases = []*Base{}
	b2, err := BaseFromEntry("git::https://localhost/group2/project.git//base?ref=master")
	if err != nil {
		t.Errorf("Must return nil, but got error: %v", err)
	}
	r.AddBase(b2)
	if len(r.bases) != 1 {
		t.Errorf("Must return 1, but got %d", len(r.bases))
	}
	if r.bases[0].String() != "git::https://localhost/group2/project.git//base?ref=master" {
		t.Errorf("Unexpected result: %s", r.bases[0])
	}
	r.AddBase(b2)
	if len(r.bases) != 1 {
		t.Errorf("Must return 1, but got %d", len(r.bases))
	}
	b3, err := BaseFromEntry("git::https://localhost/group2/foobar.git//base?ref=master")
	r.AddBase(b3)
	if len(r.bases) != 1 {
		t.Errorf("Must return 1, but got %d", len(r.bases))
	}
}

func TestGetProject(t *testing.T) {
	r := &Resolver{
		projects: map[string]*Project{
			"namespace": &Project{
				Base: &Base{
					Namespace: "namespace",
				},
			},
		},
	}
	p1 := r.GetProject("test")
	if p1 != nil {
		t.Errorf("Must return nil, but got %v", p1)
	}
	p2 := r.GetProject("namespace")
	if p2 == nil {
		t.Errorf("Must return object, but got %v", p2)
	}
}

func TestIsExcludedEntry(t *testing.T) {
	r := &Resolver{
		profile: &ResolverProfile{
			BasesDefault: []string{
				"base",
			},
		},
	}
	if !r.IsExcludedEntry("base") {
		t.Error("Must return true, but got false")
	}
	if r.IsExcludedEntry("http://localhost/base") {
		t.Error("Must return false, but got true")
	}
	if r.IsExcludedEntry("http://localhost/fase") {
		t.Error("Must return false, but got true")
	}
}

func TestAddProject(t *testing.T) {
	r := &Resolver{
		projects: make(map[string]*Project),
	}
	project := &Project{
		Base: &Base{
			Name: "serviceA",
		},
	}
	r.AddProject(project)
	if len(r.projects) != 1 {
		t.Errorf("Must return 1, but got %d", len(r.projects))
	}
	r.AddProject(project)
	if len(r.projects) != 1 {
		t.Errorf("Must return 1, but got %d", len(r.projects))
	}
}

func TestDependsOn(t *testing.T) {
	project := &Project{
		Base: &Base{
			Name: "serviceA",
		},
		Links: []*Base{
			&Base{
				Name: "serviceB",
			},
		},
	}
	r := &Resolver{
		projects: make(map[string]*Project),
	}
	r.AddProject(project)
	if d1 := r.DependsOn("serviceA"); len(d1) != 0 {
		t.Errorf("Must return 0, but got %d", len(d1))
	}
	if d1 := r.DependsOn("serviceB"); len(d1) != 1 {
		t.Errorf("Must return 0, but got %d", len(d1))
	}
}
