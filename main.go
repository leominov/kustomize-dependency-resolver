package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

var (
	logLevel          = flag.String("log-level", "info", "Logging level")
	kustomizationFile = flag.String("kustomization", fmt.Sprintf("kustomize/%s", KustomizationFilename), "Path to kustomization.yaml file")
	exclude           = flag.String("exclude", "", "Additional exclude filter")
	profile           = flag.String("profile", "", "Path to profile file or name of built-in profile")
	dependsOn         = flag.String("depends-on", "", "Prints linked services")
	showProfiles      = flag.Bool("profiles", false, "Prints list of available built-in profiles")
	saveResult        = flag.Bool("save", false, "Save result on disk")
	showResult        = flag.Bool("print", false, "Prints content of generated kustomization.yaml file(s)")
	promptPassword    = flag.Bool("password", true, "Prompt for GitLab login and password if they not specified in profile or environment")
	version           = flag.Bool("version", false, "Prints version and exit")
)

func realMain() int {
	flag.Parse()

	if *version {
		fmt.Println(Version)
		return 0
	}

	if *showProfiles {
		fmt.Println(BuiltinProfilesNames())
		return 0
	}

	level, err := logrus.ParseLevel(*logLevel)
	if err == nil {
		logrus.SetLevel(level)
	}

	runner := &Runner{
		KustomizationFile: *kustomizationFile,
		Exclude:           *exclude,
		Profile:           *profile,
		DependsOn:         *dependsOn,
		ShowResult:        *showResult,
		SaveResult:        *saveResult,
	}

	if !runner.IsActionSpecified() {
		flag.Usage()
		return 0
	}

	if err := runner.Setup(*promptPassword); err != nil {
		logrus.Error(err)
		return 1
	}

	if err := runner.Run(); err != nil {
		logrus.Error(err)
		return 1
	}

	return 0
}

func main() {
	os.Exit(realMain())
}
