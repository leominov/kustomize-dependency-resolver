.PHONY: test
test:
	go test -cover

.PHONY: test-report
test-report:
	go test -coverprofile=cover.out && go tool cover -html=cover.out

.PHONY: build
build:
	mkdir -p .build/darwin-amd64/
	GOOS=darwin GOARCH=amd64 go build -o .build/darwin-amd64/kustomize-dependency-resolver ./
	mkdir -p .build/linux-amd64/
	GOOS=linux GOARCH=amd64 go build -o .build/linux-amd64/kustomize-dependency-resolver ./
	mkdir -p .build/windows-amd64/
	GOOS=windows GOARCH=amd64 go build -o .build/windows-amd64/kustomize-dependency-resolver ./

.PHONY: build-pfa
build-pfa:
	mkdir -p .build/darwin-amd64/
	GOOS=darwin GOARCH=amd64 go build --tags pfa -o .build/darwin-amd64/kustomize-dependency-resolver ./
	mkdir -p .build/linux-amd64/
	GOOS=linux GOARCH=amd64 go build --tags pfa -o .build/linux-amd64/kustomize-dependency-resolver ./
	mkdir -p .build/windows-amd64/
	GOOS=windows GOARCH=amd64 go build --tags pfa -o .build/windows-amd64/kustomize-dependency-resolver ./
