package main

import (
	"io/ioutil"
	"os"
	"path"

	"gopkg.in/yaml.v2"
)

const (
	KustomizationFilename = "kustomization.yaml"
)

type KustomizationFile struct {
	GeneratorOptions *GeneratorOptions `yaml:"generatorOptions"`
	Bases            []string          `yaml:"bases"`
}

type GeneratorOptions struct {
	DisableHash bool `yaml:"disableHash"`
}

func (k *KustomizationFile) Parse(b []byte) error {
	err := yaml.Unmarshal(b, &k)
	if err != nil {
		return err
	}
	return nil
}

func (k *KustomizationFile) SaveTo(file string) error {
	dir := path.Dir(file)
	if len(dir) > 0 {
		err := os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			return err
		}
	}
	outString, err := k.String()
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(file, []byte(outString), os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

func (k *KustomizationFile) String() (string, error) {
	out, err := yaml.Marshal(k)
	if err != nil {
		return "", err
	}
	return string(out), nil
}
