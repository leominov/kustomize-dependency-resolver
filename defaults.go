package main

import "strings"

func GetBuiltinProfile(name string) (*ResolverProfile, bool) {
	if BuiltinProfiles == nil {
		return nil, false
	}
	if p, ok := BuiltinProfiles[name]; ok {
		return p, true
	}
	return nil, false
}

func BuiltinProfilesNames() string {
	if BuiltinProfiles == nil {
		return "<none>"
	}
	var result []string
	for name := range BuiltinProfiles {
		result = append(result, name)
	}
	return strings.Join(result, ", ")
}
